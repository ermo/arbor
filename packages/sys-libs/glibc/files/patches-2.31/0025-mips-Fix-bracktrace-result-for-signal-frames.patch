Upstream: yes, taken from release/2.31/master

From 54ba2541b3a76441a9cbe5dd14c963bf874fd5e9 Mon Sep 17 00:00:00 2001
From: Adhemerval Zanella <adhemerval.zanella@linaro.org>
Date: Mon, 17 Feb 2020 10:57:56 -0300
Subject: [PATCH 25/44] mips: Fix bracktrace result for signal frames

MIPS fallback code handle a frame where its FDE can not be obtained
(for instance a signal frame) by reading the kernel allocated signal frame
and adding '2' to the value of 'sc_pc' [1].  The added value is used to
recognize an end of an EH region on mips16 [2].

The fix adjust the obtained signal frame value and remove the libgcc added
value by checking if the previous frame is a signal frame one.

Checked with backtrace and tst-sigcontext-get_pc tests on mips-linux-gnu
and mips64-linux-gnu.

[1] libgcc/config/mips/linux-unwind.h from gcc code.
[2] gcc/config/mips/mips.h from gcc code.  */

(cherry picked from commit 6e05978f0c30e52420e086cc3156655471e6fb0a)
---
 debug/backtrace.c                          |  5 ++
 sysdeps/generic/unwind-arch.h              | 30 ++++++++++
 sysdeps/unix/sysv/linux/mips/unwind-arch.h | 67 ++++++++++++++++++++++
 3 files changed, 102 insertions(+)
 create mode 100644 sysdeps/generic/unwind-arch.h
 create mode 100644 sysdeps/unix/sysv/linux/mips/unwind-arch.h

diff --git a/debug/backtrace.c b/debug/backtrace.c
index cc4b9a5c90..69cf4c23c8 100644
--- a/debug/backtrace.c
+++ b/debug/backtrace.c
@@ -23,6 +23,7 @@
 #include <gnu/lib-names.h>
 #include <stdlib.h>
 #include <unwind.h>
+#include <unwind-arch.h>
 
 struct trace_arg
 {
@@ -78,6 +79,10 @@ backtrace_helper (struct _Unwind_Context *ctx, void *a)
   if (arg->cnt != -1)
     {
       arg->array[arg->cnt] = (void *) unwind_getip (ctx);
+      if (arg->cnt > 0)
+	arg->array[arg->cnt]
+	  = unwind_arch_adjustment (arg->array[arg->cnt - 1],
+				    arg->array[arg->cnt]);
 
       /* Check whether we make any progress.  */
       _Unwind_Word cfa = unwind_getcfa (ctx);
diff --git a/sysdeps/generic/unwind-arch.h b/sysdeps/generic/unwind-arch.h
new file mode 100644
index 0000000000..d712e5e11d
--- /dev/null
+++ b/sysdeps/generic/unwind-arch.h
@@ -0,0 +1,30 @@
+/* Return backtrace of current program state.  Arch-specific bits.
+   Copyright (C) 2020 Free Software Foundation, Inc.
+   This file is part of the GNU C Library.
+
+   The GNU C Library is free software; you can redistribute it and/or
+   modify it under the terms of the GNU Lesser General Public
+   License as published by the Free Software Foundation; either
+   version 2.1 of the License, or (at your option) any later version.
+
+   The GNU C Library is distributed in the hope that it will be useful,
+   but WITHOUT ANY WARRANTY; without even the implied warranty of
+   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+   Lesser General Public License for more details.
+
+   You should have received a copy of the GNU Lesser General Public
+   License along with the GNU C Library; if not, see
+   <https://www.gnu.org/licenses/>.  */
+
+#ifndef _UNWIND_ARCH_H
+#define _UNWIND_ARCH_H
+
+#include <unwind.h>
+
+static inline void *
+unwind_arch_adjustment (void *prev, void *addr)
+{
+  return addr;
+}
+
+#endif
diff --git a/sysdeps/unix/sysv/linux/mips/unwind-arch.h b/sysdeps/unix/sysv/linux/mips/unwind-arch.h
new file mode 100644
index 0000000000..a009899983
--- /dev/null
+++ b/sysdeps/unix/sysv/linux/mips/unwind-arch.h
@@ -0,0 +1,67 @@
+/* Return backtrace of current program state.  Arch-specific bits.
+   Copyright (C) 2020 Free Software Foundation, Inc.
+   This file is part of the GNU C Library.
+
+   The GNU C Library is free software; you can redistribute it and/or
+   modify it under the terms of the GNU Lesser General Public
+   License as published by the Free Software Foundation; either
+   version 2.1 of the License, or (at your option) any later version.
+
+   The GNU C Library is distributed in the hope that it will be useful,
+   but WITHOUT ANY WARRANTY; without even the implied warranty of
+   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+   Lesser General Public License for more details.
+
+   You should have received a copy of the GNU Lesser General Public
+   License along with the GNU C Library; if not, see
+   <https://www.gnu.org/licenses/>.  */
+
+#ifndef _UNWIND_ARCH_H
+#define _UNWIND_ARCH_H
+
+#include <stdint.h>
+
+/* MIPS fallback code handle a frame where its FDE can not be obtained
+   (for instance a signal frame) by reading the kernel allocated signal frame
+   and adding '2' to the value of 'sc_pc' [1].  The added value is used to
+   recognize an end of an EH region on mips16 [2].
+
+   The idea here is to adjust the obtained signal frame ADDR value and remove
+   the libgcc added value by checking if the previous frame is a signal frame
+   one.
+
+   [1] libgcc/config/mips/linux-unwind.h from gcc code.
+   [2] gcc/config/mips/mips.h from gcc code.  */
+
+static inline void *
+unwind_arch_adjustment (void *prev, void *addr)
+{
+  uint32_t *pc = (uint32_t *) prev;
+
+  if (pc == NULL)
+    return addr;
+
+  /* For MIPS16 or microMIPS frame libgcc makes no adjustment.  */
+  if ((uintptr_t) pc & 0x3)
+    return addr;
+
+  /* The vDSO containes either
+
+     24021061 li v0, 0x1061 (rt_sigreturn)
+     0000000c syscall
+	or
+     24021017 li v0, 0x1017 (sigreturn)
+     0000000c syscall  */
+  if (pc[1] != 0x0000000c)
+    return addr;
+#if _MIPS_SIM == _ABIO32
+  if (pc[0] == (0x24020000 | __NR_sigreturn))
+    return (void *) ((uintptr_t) addr - 2);
+#endif
+  if (pc[0] == (0x24020000 | __NR_rt_sigreturn))
+    return (void *) ((uintptr_t) addr - 2);
+
+  return addr;
+}
+
+#endif
-- 
2.27.0

